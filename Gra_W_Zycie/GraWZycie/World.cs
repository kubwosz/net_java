﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GraWZycie
{
    class World
    {
        static int N = 10;
        static int M = 15;

        public int[,] tab = {{ 1, 1, 0, 0, 1 },
        { 1,0,0,1,1 },
        { 1,0,0,1,1 },
        { 1,0,0,1,1 },
        { 1,0,0,1,1 }
        };

        public int[,] t = new int[N, M];

        public void FillTable()
        {
            Random rnd = new Random();
            for (int i = 0; i < t.GetLength(1); ++i)
            {
                for (int j = 0; j < t.GetLength(0); ++j)
                {
                    
                    t[j, i] = rnd.Next(0,2);
                }
            }
        }


        public void Display()
        {
            for (int i = 0; i < t.GetLength(1); ++i)
            {
                for (int j = 0; j < t.GetLength(0); ++j)
                {
                    Console.Write(t[j, i]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }

        public int NumberOfNeighbours(int x, int y)
        {
            int number = 0;


            for (int i = x - 1; i <= x + 1; ++i)
            {
                if (i == -1 || i == t.GetLength(0))
                    ;
                else
                    for (int j = y - 1; j <= y + 1; ++j)
                    {
                        if ((i == x && j == y) || j == -1 || j == t.GetLength(1))
                            ;
                        else
                        if (t[i, j] == 1)
                            ++number;
                    }

            }

            return number;
        }

        public void NewWorld()
        {
            int[,] tab2 = new int[N, M];

            
            for (int i = 0; i < t.GetLength(0); ++i)
            {
                for (int j = 0; j < t.GetLength(1); ++j)
                {
                    if(t[i,j] == 1)
                    {
                        if (NumberOfNeighbours(i, j) < 2)
                            tab2[i, j] = 0;
                        else if (NumberOfNeighbours(i, j) <= 3)
                            tab2[i, j] = 1;
                        else if(NumberOfNeighbours(i,j) > 3)
                            tab2[i, j] = 0;

                    }
                    else
                    {
                        if (NumberOfNeighbours(i, j) == 3)
                            tab2[i, j] = 1;
                    }
                }
            }

            t = tab2;
        }

    }
}
