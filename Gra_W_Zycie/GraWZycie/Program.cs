﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraWZycie
{
    class Program
    {

        static void Main(string[] args)
        {

            World world = new World();

            world.FillTable();
            world.Display();

            //Console.WriteLine(world.NumberOfNeighbours(4, 4).ToString());
            //Console.ReadLine();

            world.NewWorld();
            world.Display();

            world.NewWorld();
            world.Display();

        }
    }
}
